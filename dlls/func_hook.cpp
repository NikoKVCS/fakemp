#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "metahook.h"
#include "amx_util.h"
#include "gamerules.h"
#include "hook_config.h"

HMODULE g_hProgs;
DWORD g_dwProgsBase;

FARPROC (WINAPI *g_pfnGetProcAddress)(HMODULE hModule, LPCSTR lpProcName);
hook_t *g_phGetProcAddress;
FARPROC WINAPI Hook_GetProcAddress(HMODULE hModule, LPCSTR lpProcName);

CGameRules* (*g_pfnInstallGameRules)(void );
CGameRules* Hook_InstallGameRules(void );
hook_t *g_phInstallGameRules;

void W_Precache(void);
void Hook_W_Precache(void);
void (*g_pfnW_Precache)(void);

void (*g_pfnAddEntityHashValue)(struct entvars_s *pev, const char *value, hash_types_e fieldType);
void (*g_pfnRemoveEntityHashValue)(struct entvars_s *pev, const char *value, hash_types_e fieldType);

void (*g_pfnApplyMultiDamage)(entvars_t *pevInflictor, entvars_t *pevAttacker);
void (*g_pfnClearMultiDamage)(void);
void (*g_pfnAddMultiDamage)(entvars_t *pevInflictor, CBaseEntity *pEntity, float flDamage, int bitsDamageType);

void (*g_pfnClientPutInServer)(edict_t *pEntity);
void Hook_ClientPutInServer(edict_t *pEntity);
void ClientPutInServer(edict_t *pEntity);

void MP_InstallHooks()
{
	g_hProgs = GetModuleHandle("mp.dll");
	if(g_hProgs)
	{
		Log( "mp.dll has been loaded");
		g_dwProgsBase = (DWORD)g_hProgs;
	}
	else
		Log("mp.dll has failed to be loaded");

	g_pfnInstallGameRules = (CGameRules *(*)(void))(g_dwProgsBase + 0x76C70);
	g_phInstallGameRules = MH_InlineHook((void *)g_pfnInstallGameRules, Hook_InstallGameRules, (void *&)g_pfnInstallGameRules);

	g_phGetProcAddress = MH_InlineHook((void *)GetProcAddress, Hook_GetProcAddress, (void *&)g_pfnGetProcAddress);

	g_pfnW_Precache = (void (*)(void))(g_dwProgsBase + 0xB7850);
	MH_InlineHook((void *)g_pfnW_Precache, Hook_W_Precache, (void *&)g_pfnW_Precache);

	g_pfnAddEntityHashValue = (void (*)(struct entvars_s *, const char *, hash_types_e))(g_dwProgsBase + 0x59850);
	g_pfnRemoveEntityHashValue = (void (*)(struct entvars_s *, const char *, hash_types_e))(g_dwProgsBase + 0x59A10);

	g_pfnApplyMultiDamage = (void (*)(entvars_t *, entvars_t *))(g_dwProgsBase + 0xB7540);
	g_pfnClearMultiDamage = (void (*)(void))(g_dwProgsBase + 0xB7520);
	g_pfnAddMultiDamage = (void (*)(entvars_t *, CBaseEntity *, float , int ))(g_dwProgsBase + 0xB7570);

	g_pfnClientPutInServer = (void (*)(edict_t * ))(g_dwProgsBase + 0x5C570);
	MH_InlineHook((void *)g_pfnClientPutInServer, Hook_ClientPutInServer, (void *&)g_pfnClientPutInServer);
}

void MP_UninstallHook(void)
{
	MH_FreeAllHook();
}

CGameRules* Hook_InstallGameRules(void )
{
	g_pGameRules = (CHalfLifeMultiplay *)g_pfnInstallGameRules();
	return g_pGameRules;
}

void Hook_W_Precache(void)
{
	g_pfnW_Precache();
	W_Precache();

	// On world precaching
	// you could precache your resources below here
}

void Hook_ClientPutInServer(edict_t *pEntity)
{
#ifdef _USE_FAKE_CBASEPLAYER
	ClientPutInServer(pEntity);
#else
	g_pfnClientPutInServer(pEntity);
#endif
}