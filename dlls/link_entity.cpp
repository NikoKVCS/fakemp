#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "metahook.h"
#include "amx_util.h"

extern HMODULE g_hProgs;
extern FARPROC (WINAPI *g_pfnGetProcAddress)(HMODULE hModule, LPCSTR lpProcName);

// use this form to declare the entity export functions
// for example : LINK_ENTITY_TO_CLASS(weapon_ak47, CAK47); then you just need to code like this:
extern "C" _declspec(dllexport) void weapon_ak47(entvars_t *pev);
extern "C" _declspec(dllexport) void weapon_awp(entvars_t *pev);
extern "C" _declspec(dllexport) void weapon_usp(entvars_t *pev);

FARPROC WINAPI Hook_GetProcAddress(HMODULE hModule, LPCSTR lpProcName)
{
	if(g_hProgs != hModule)
		return g_pfnGetProcAddress(hModule, lpProcName);

	// here is an example of installing a entity link.
	// And the original export function with the same name in mp.dll will be replaced
	if (  !lstrcmp(lpProcName, "weapon_ak47"))
		return (FARPROC)weapon_ak47;
	if (  !lstrcmp(lpProcName, "weapon_awp"))
		return (FARPROC)weapon_awp;
	if (  !lstrcmp(lpProcName, "weapon_usp"))
		return (FARPROC)weapon_usp;

	return g_pfnGetProcAddress(hModule, lpProcName);
}