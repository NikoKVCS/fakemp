#include "amxxmodule.h"
#include "metahook.h"
#include "amx_util.h"
#include "func_hook.h"

void OnAmxxAttach()
{
	MP_InstallHooks();
}

void OnAmxxDetach()
{
	MP_UninstallHook();
}

void OnPluginsLoaded()
{
}