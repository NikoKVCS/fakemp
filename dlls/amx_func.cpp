#include "amxxmodule.h"
#include "metahook.h"
#include "amx_util.h"
#include "gamerules.h"

int DispatchSpawn(edict_t *pent)
{
	RETURN_META_VALUE(MRES_IGNORED, 0);
}

void LinkUserMessages(void);

BOOL FN_ClientConnect_Post(edict_t *pEntity, const char *pszName, const char *pszAddress, char szRejectReason[ 128 ])
{
	LinkUserMessages();
	RETURN_META_VALUE(MRES_IGNORED, 1);
}